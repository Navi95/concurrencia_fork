#include "csapp.h"

void echo(int connfd);
int parseline(char *buf, char **argv);/*Sacado del libro*/
int builtin_command(char **argv);/*Sacado del libro*/


void sigchild_handler(int sig){

	while(waitpid(-1,0,WNOHANG)>0);
	return;
}




int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;


	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	Signal(SIGCHLD, sigchild_handler);
	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		
		if(fork()==0){
			close(listenfd);
			echo(connfd);
			Close(connfd);
			exit(0);

		
		}
		Close(connfd);
			
	}
}

void echo(int connfd)
{
	char buf[MAXLINE];
	size_t n;
	rio_t rio;
	pid_t pid;
	int bg;
	int st;
	char *argv[128];
	char *ok="OK\n";
	char *error= "Comando Erroneo.\n";

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		bg = parseline(buf, argv);
		
		if(!builtin_command(argv)){
			printf("\nCOMMAND RECIVED: %s\n\n",buf);
			if ((pid=fork())==0){
				if(execve(argv[0],argv,environ)<0){
					printf("%s: Commando Erroneo.\n",buf);
					exit(1);					
				}
			}else{
				waitpid(pid,&st,0);
				if (st!=0){
					Rio_writen(connfd, error, strlen(error));
				}
				else{
					Rio_writen(connfd, ok, strlen(ok));
				}
			}		
		}	
	}

}

int parseline(char *buf, char **argv)
{
	char *delim;
	int argc;
	int bg;


	buf[strlen(buf)-1] = ' '; 
	while (*buf && (*buf == ' ')) 
		buf++;
	argc = 0;
	while ((delim = strchr(buf, ' '))) {
		argv[argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) 
			buf++;
	}
	argv[argc] = NULL;

	if (argc == 0)
		return 1;

	if ((bg = (*argv[argc-1] == '&')) != 0)
		argv[--argc] = NULL;

	return bg;

}

int builtin_command(char **argv)
{
	if (!strcmp(argv[0], "quit"))
		exit(0);
	if (!strcmp(argv[0], "&"))
		return 1;
	return 0;

}
